/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package jmad.modelpack.psbinjection;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;

import cern.accsoft.steering.jmad.domain.beam.Beam.Particle;
import cern.accsoft.steering.jmad.tools.modeldefs.creating.lang.JMadModelDefinitionDslSupport;

public class PsbInjectionModelDefinitionFactory extends JMadModelDefinitionDslSupport {
    {
        name("PsbInjection");

        init(i -> {
            i.call("strength/lt.str");
            i.call("elements/lt.ele");
            i.call("sequence/lt.seq");
            i.call("aperture/lt.dbx");
            i.call("strength/ltb.str");
            i.call("elements/ltb.ele");
            i.call("sequence/ltb.seq");
            i.call("aperture/ltb.dbx");
            i.call("strength/bi.str");
            i.call("elements/bi.ele");
            i.call("aperture/bi.dbx");
            i.call("sequence/BI1.seq");
            i.call("sequence/BI2.seq");
            i.call("sequence/BI3.seq");
            i.call("sequence/BI4.seq");
            i.call("jmad/ltltbbi.madx");
        });
        
        sequence("ltltbbi1").isDefault().isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.deltap(0.0);
                    t.betx(1.342);
                    t.alfx(-0.190);
                    t.bety(2.48);
                    t.alfy(0.103);
                });
            });
            s.beam(b->{
                b.particle(Particle.PROTON);
                b.pc(0.371039285);
                b.exn(15E-6*3.0);
                b.eyn(8E-6*3.0);
                b.sige(1.35E-3*3.0);
                b.sigt(230E-9);
            });
        });
        
        sequence("ltltbbi2").isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.deltap(0.0);
                    t.betx(1.342);
                    t.alfx(-0.190);
                    t.bety(2.48);
                    t.alfy(0.103);
                });
            });
            s.beam(b->{
                b.particle(Particle.PROTON);
                b.pc(0.371039285);
                b.exn(15E-6*3.0);
                b.eyn(8E-6*3.0);
                b.sige(1.35E-3*3.0);
                b.sigt(230E-9);
            });
        });
        
        sequence("ltltbbi3").isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.deltap(0.0);
                    t.betx(1.342);
                    t.alfx(-0.190);
                    t.bety(2.48);
                    t.alfy(0.103);
                });
            });
            s.beam(b->{
                b.particle(Particle.PROTON);
                b.pc(0.371039285);
                b.exn(15E-6*3.0);
                b.eyn(8E-6*3.0);
                b.sige(1.35E-3*3.0);
                b.sigt(230E-9);
            });
        });
        
        sequence("ltltbbi4").isDefinedAs(s -> {
            s.range("ALL").isDefault().isDefinedAs(r -> {
                r.twiss(t -> {
                    t.deltap(0.0);
                    t.betx(1.342);
                    t.alfx(-0.190);
                    t.bety(2.48);
                    t.alfy(0.103);
                });
            });
            s.beam(b->{
                b.particle(Particle.PROTON);
                b.pc(0.371039285);
                b.exn(15E-6*3.0);
                b.eyn(8E-6*3.0);
                b.sige(1.35E-3*3.0);
                b.sigt(230E-9);
            });
        });
       
        
        optics("psbInjection_2018").isDefault().isDefinedAs(o -> {
            o.call("strength/lt.str").parseAs(STRENGTHS);
            o.call("strength/ltb.str").parseAs(STRENGTHS);
            o.call("strength/bi.str").parseAs(STRENGTHS);
        });
        
        
    }
}
