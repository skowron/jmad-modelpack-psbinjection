/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package jmad.modelpack.psbinjection;

import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class PsbInjectionModelDefinitionCreator {
    public static void main(String[] args) {
        scanDefault().and().writeTo("src/java/jmad/modelpack/psbinjection");
    }

}
