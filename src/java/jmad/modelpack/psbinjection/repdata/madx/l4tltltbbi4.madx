/*****************************************************
 *
 * MADX file for the L4T-LT-LTB-BI line from LINAC4 to PSB Injection
 *
 *
 * Execute with:  >madx < l4t_lt_ltb_bi.madx
 * This file is for H- at Ekin=160MeV
 *****************************************************/

/*****************************************************************************
 * TITLE
 *****************************************************************************/
ring = 4;  

value, ring;

if (ring == 1) { title, 'L4T-LT-LTB-BI1 postLS2 optics. H- E_kin=160 MeV'; }
elseif (ring == 2) { title, 'L4T-LT-LTB-BI2 postLS2 optics. H- E_kin=160 MeV'; }
elseif (ring == 4) { title, 'L4T-LT-LTB-BI4 postLS2 optics. H- E_kin=160 MeV'; }
else { title, 'L4T-LT-LTB-BI3 postLS2 optics. H- E_kin=160 MeV'; }


 option, -echo;
!option, RBARC=FALSE;

/*****************************************************************************
 * L4T-LT-LTB-BI line
 * NB! The order of file loading matters:
 *     Use like this :   .ele > .seq > _ele.str > .str > .dbx   
 *     The reason is a >feature< of MADX
 *
 *****************************************************************************/

 call, file = '../elements/l4t.ele';
 call, file = '../elements/lt.ele';
 call, file = '../elements/ltb.ele';
 call, file = '../elements/bi.ele';

 call, file = '../sequence/l4t.seq';
 call, file = '../sequence/lt.seq';
 call, file = '../sequence/ltb.seq';
 call, file = '../sequence/bi.seq';

! here it is where the bendings are defined
 call, file = '../strength/l4t_ele.str';
 call, file = '../strength/lt_ele.str';
 call, file = '../strength/ltb_ele.str';

! here it is where the quads are defined
 call, file = '../strength/l4t.str';
 call, file = '../strength/lt.str';
 call, file = '../strength/ltb.str';
 call, file = '../strength/bi.str';
 
 
 call, file = '../strength/debuncherPlus1000kV.str';

 !call, file = '../aperture/l4t.dbx';
 !call, file = '../aperture/lt.dbx';
 !call, file = '../apperture/ltb.dbx';
 !call, file = '../apperture/bi.dbx';

/*****************************************************************************
 * build up the geometry of the beam lines
 *****************************************************************************/




len_L4T = 70.12612;
len_LT  = 29.928517649;
len_LTB = 29.16156022; 
len_BI1 = 48.915331;
len_BI2 = 48.882535;
len_BI3 = 48.86322;
len_BI4 = 48.882786;

l4tltltbbi1: sequence, refer=ENTRY,  l = len_L4T+len_LT+len_LTB+len_BI1 ;
  l4t                         , at = 0;
  lt                          , at = len_L4T;
  ltb                         , at = len_L4T + len_LT ;
  bi1                         , at = len_L4T + len_LT + len_LTB ;
endsequence;
save, sequence=l4tltltbbi1, file=./l4tltltbbi1_sav.seq;


l4tltltbbi2: sequence, refer=ENTRY,  l = len_L4T+len_LT+len_LTB+len_BI2 ;
  l4t                         , at = 0;
  lt                          , at = len_L4T;
  ltb                         , at = len_L4T + len_LT ;
  bi2                         , at = len_L4T + len_LT + len_LTB ;
endsequence;
save, sequence=l4tltltbbi2, file=./l4tltltbbi2_sav.seq;


l4tltltbbi3: sequence, refer=ENTRY,  l = len_L4T+len_LT+len_LTB+len_BI3 ;
  l4t                         , at = 0;
  lt                          , at = len_L4T;
  ltb                         , at = len_L4T + len_LT ;
  bi3                         , at = len_L4T + len_LT + len_LTB ;
endsequence;
save, sequence=l4tltltbbi3, file=./l4tltltbbi3_sav.seq;


l4tltltbbi4: sequence, refer=ENTRY,  l = len_L4T+len_LT+len_LTB+len_BI4 ;
  l4t                         , at = 0;
  lt                          , at = len_L4T;
  ltb                         , at = len_L4T + len_LT ;
  bi4                         , at = len_L4T + len_LT + len_LTB ;
endsequence;
save, sequence=l4tltltbbi4, file=./l4tltltbbi4_sav.seq;




/*****************************************************************************
 * set initial twiss parameters
 *****************************************************************************/
call, file = '../inp/l4t.inp';

/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;

/*******************************************************************************
 * Beam
 * NB! beam->ex == (beam->exn)/(beam->gamma*beam->beta*4)
 *******************************************************************************/
 BEAM, PARTICLE=Hminus, MASS=.93929, CHARGE=-1, PC=0.57083;! ENERGY=1.09929;
!show, beam;




/*******************************************************************************
 * Use
 *******************************************************************************/

if (ring == 1) { use, sequence=l4tltltbbi1; }
elseif (ring == 2) { use, sequence=l4tltltbbi2; }
elseif (ring == 4) { use, sequence=l4tltltbbi4; }
else { use, sequence=l4tltltbbi3; }


/*******************************************************************************
 * twiss
 *******************************************************************************/

maketwiss: macro=
 {
     ptc_create_universe;
     ptc_create_layout,model=2,method=6,nst=5,exact,time=true;
     ptc_setswitch, debuglevel=1, time=true, fringe=true, nocavity=false;
     ptc_twiss, table=ptc_twiss, BETA0=initbeta0, betz=0.1, icase=6, no=3,file="twiss";
     ptc_end;

 };

exec, maketwiss;

write, table=ptc_twiss, file="twiss.ptc.tfs";

!plot,table=ptc_twiss,haxis=s, vaxis=beta11,beta22, colour=100;
!plot,table=ptc_twiss,haxis=s, vaxis=disp1,disp3, colour=100;


!value,table(ptc_twiss,LTLTBBI$START,alfa11);
!value,table(ptc_twiss,LT.DHZ10,beta11);


/***************************************************
 * Write ptc_twiss table. NB! Values at end of elements
 ***************************************************/
set,  format="-18s";
set,  format="10.5f";

! ns    ~ longitudinal number of sigma
! nt    ~ transverse   number of sigma
! dimxc ~ size-x with dispersion, dimyc ~ size-y with dispersion,
! value, beam->ex, beam->ey, beam->sige;

! ns = 2;
! nt = 3;
! dimxc := sqrt( nt^2*table(ptc_twiss,beta11)*beam->ex + ns^2*(beam->sige*table(ptc_twiss,disp1))^2 );
! dimyc := sqrt( nt^2*table(ptc_twiss,beta22)*beam->ey + ns^2*(beam->sige*table(ptc_twiss,disp3))^2 );
!
! exec, maketwiss; ! Needs to be executed first, to make beta11, disp1, etc.
! select,flag=ptc_twiss,clear;
! select,flag=ptc_twiss, column = name,s,l,alfa11,beta11,disp1,dimxc,alfa22,beta22,disp3,qdimyc,mu1,mu2;
! exec, maketwiss;
! write,table=ptc_twiss,file="../out/l4tltltblbe.out"


 select,flag=ptc_twiss,clear;
 ! the one to be used for YASP output
 !select,flag=ptc_twiss, column = name,angle,k1L,k2L,k3L,beta11,beta22,disp1,disp3,x,y,alfa11,alfa22,mu1,mu2,disp2,disp4,px,py;
select,flag=ptc_twiss, column = name,s,angle,k1L,k2L,k3L,beta11,beta22,disp1,disp3,x,y,alfa11,alfa22,mu1,mu2,disp2,disp4,px,py;
 exec, maketwiss;
if (ring == 1) { write,table=ptc_twiss,file="../yasp/l4tltltbbi1.out"; }
elseif (ring == 2) { write,table=ptc_twiss,file="../yasp/l4tltltbbi2.out"; }
elseif (ring == 4) { write,table=ptc_twiss,file="../yasp/l4tltltbbi4.out"; }
else { write,table=ptc_twiss,file="../yasp/l4tltltbbi3.out"; }


/*******************************************************************************
 * Plot l4tltltbbiX
 *******************************************************************************/

option, -info;
option, -echo;

resplot;
setplot, post=2;

if (ring == 1) {
   plot, title='bi1'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , style:=100,symbol:=4,colour=100
                           , file = "l4tltltbbi1";

   plot, title='bi1'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , vaxis2=disp1
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;

   plot, title='bi1'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=disp1,disp3
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;
} elseif (ring == 2) {
   plot, title='bi2'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , style:=100,symbol:=4,colour=100
                           , file = "l4tltltbbi2";

   plot, title='bi2'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , vaxis2=disp1
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;

   plot, title='bi2'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=disp1,disp3
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;
}  
elseif (ring == 4) {
   plot, title='bi4'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , style:=100,symbol:=4,colour=100
                           , file = "l4tltltbbi4";

   plot, title='bi4'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , vaxis2=disp1
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;

   plot, title='bi4'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=disp1,disp3
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;
}   
else {
   plot, title='bi3'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , style:=100,symbol:=4,colour=100
                           , file = "l4tltltbbi3";

   plot, title='bi3'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=beta11,beta22
                           , vaxis2=disp1
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;

   plot, title='bi3'       , table=ptc_twiss
                           , haxis=s
                           , vaxis1=disp1,disp3
                           , range=#S/#e
                           , style:=100, symbol:=4, colour=100;
}  

/*******************************************************************************
 * Clean up 
 *******************************************************************************/
system, "rm internal_mag_pot.txt";


stop;
